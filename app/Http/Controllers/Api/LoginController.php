<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
class LoginController extends Controller
{
    public function login(Request $request){
      $this->validateLogin($request);

      //si entra
      if(Auth::attempt($request->only('email','password'))){
        return response()->json([
                'token' => $request->user()->createToken($request->name)->plainTextToken,
                'message' => 'Success'
            ]);
      }
      //si no entra
      return response()->json([
        'message'=>'Error al conectarse'
      ],401);
    }

    public function validateLogin(Request $request){
      return $request->validate([
        'email'=>'email|required',
        'password'=>'required',
        'name'=>'required'
      ]);
    }
}
