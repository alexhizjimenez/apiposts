<?php

namespace App\Http\Resources\Api\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'title_post'=>$this->title,
          'slug'=>$this->slug,
          'excerpt'=>$this->excerpt,
          'published'=>$this->published
        ];
    }
}
