<?php

namespace App\Http\Resources\Api\V2;

use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
  
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'title'=>$this->title,
          'excerpt'=>$this->excerpt,
          'published'=>$this->published,
          'user'=>[
            'name'=>$this->user->name,
            'email'=>$this->user->email,
          ],
        ];
    }
}
